const postToHoneywell = options => {}

const postTemp = ({ heatSetpoint, coolSetpoint, mode }) => {
  postToHoneywell({
    heatSetpoint,
    coolSetpoint,
    mode
    /* thermostatSetpointStatus or autoChangeoverActive depending on TCC or LCC device.
     * or maybe nothing, the docs aren't crazy clear */
  })
}

const getCurrentTemp = () => 76

module.exports = {
  postTemp,
  getCurrentTemp
}
