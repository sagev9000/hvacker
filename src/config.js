const path = require('path')
const homedir = require('os').homedir()
const fs = require('fs')

const fileName = 'hvackerconfig.json'

const configPaths = [
  path.join('/secrets', fileName),
  path.join(homedir, '.add123', fileName)
]

const getConfigData = () => {
  const configPath = configPaths.find(fs.existsSync)
  if (!configPath) {
    throw 'Could not find a config file!'
  }
  const config = fs.readFileSync(configPath, 'utf8')
  return JSON.parse(config)
}

module.exports = getConfigData()
