module.exports = {
  leaderBoardViewer: {
    name: 'Leaderboard-Viewer',
    description: 'Thank you for viewing the leaderboard!',
    emoji: 'trophy'
  },
  seeTheQuade: {
    name: 'See the Quade',
    description: 'Quade has appeared in your buyables',
    emoji: 'quade'
  },
  greenCoin: {
    name: 'Lucky Green Coin',
    description: 'Its wings smell like onions',
    emoji: 'money_with_wings'
  },
  goldBrick: {
    name: 'The Golden Brick',
    description: 'Find a lucky gold brick',
    emoji: 'goldbrick'
  },
  luckyGem: {
    name: 'Lucky Gem Acquired',
    description: 'It sparkles',
    emoji: 'gem'
  },
  bugFinder: {
    name: 'Bug-Finder',
    description: 'You must have a magnifying glass or something',
    emoji: 'bug'
  },
  bigBets: {
    name: 'Make a bet over 100B',
    description: 'I like big bets, and that\'s the truth',
    emoji: 'slot_machine'
  },
  hugeBets: {
    name: 'Make a bet over 100T',
    description: `That's so bonk`,
    emoji: 'game_die'
  },
  mondoBets: {
    name: 'Make a bet over 100 Quadrillion',
    description: 'H I G H  R O L L E R',
    emoji: '8ball'
  },
  sigmaBets: {
    name: 'Make a bet over 100 Quintillion',
    description: 'Return to monke',
    emoji: 'yeknom'
  },
  ignited: {
    name: 'You light my fire, baby',
    description: 'And you pay attention to descriptions!',
    emoji: 'fire'
  },

  ratGod: {
    name: 'Own 100 Mice',
    description: 'I\'m beginning to feel like a rat god, rat god.',
    emoji: 'mouse2'
  },
  mathematician: {
    name: 'Own 100 Accountants',
    description: 'They rejoice at the appearance of a third digit.',
    emoji: 'male-office-worker'
  },
  iPod: {
    name: 'Own 100 Whales',
    description: `With the new iPod, you can hold 100's of songs.`,
    emoji: 'whale'
  },
  fire100: {
    name: 'Own 100 Fires',
    description: `Wow, that's bright.`,
    emoji: 'fire'
  },
  train100: {
    name: 'Own 100 Trains',
    description: `That's every train in America you've got there.`,
    emoji: 'train2'
  },
  boom100: {
    name: 'Own 100 Boomerangs',
    description: `LOUD WOOSHING`,
    emoji: 'boomerang'
  },
  moon100: {
    name: 'Own 100 Moons',
    description: `Space Cadet`,
    emoji: 'new_moon_with_face'
  },
  mirror100: {
    name: 'Own 100 Mirrors',
    description: `Disco Ball`,
    emoji: 'mirror'
  },
  butterfly100: {
    name: 'Own 100 Butterflies',
    description: `Delicate yet powerful.`,
    emoji: 'butterfly'
  },
  quade100: {
    name: 'Own 100 Quades',
    description: `Your Ops are super Devved right now.`,
    emoji: 'quade'
  },
  hvacker100: {
    name: 'Own 100 Hvackers',
    description: `Did Sage finally make his git repo public?`,
    emoji: 'hvacker_angery'
  },
  creator100: {
    name: 'Own 100 Creators',
    description: `_Stern look_`,
    emoji: 'question'
  },
  smallBusiness100: {
    name: 'Own 100 Small Businesses',
    description: `Enough to run a small city.`,
    emoji: 'convenience_store'
  },
  bigBusiness100: {
    name: 'Own 100 Big Businesses',
    description: `I mean... that's basically all of them.`,
    emoji: 'office'
  },
  government100: {
    name: 'Run 100 Governments',
    description: `I hope you're using them for something good...`,
    emoji: 'japanese_castle'
  },

  weAllNeedHelp: {
    name: 'View the \'!coin\' help',
    description: 'We all need a little help sometimes',
    emoji: 'grey_question'
  },
  showReverence: { // Not implemented
    name: 'Show your reverence in the chat',
    description: 'What a good little worshipper.',
    emoji: 'blush'
  },
  walmartGiftCard: {
    name: 'Walmart Gift Card',
    description: 'May or may not be expired',
    emoji: 'credit_card'
  },

  hvackerAfterDark: {
    name: 'Hvacker after dark',
    description: 'You might be taking this a little far.',
    emoji: 'night_with_stars'
  },
  certifiedCoolGuy: {
    name: 'Certified Cool Guy',
    description: 'You absolutely know how to party.',
    emoji: 'sunglasses'
  },
  itsOverNineHundred: {
    name: 'Play the HVAC game 1000 times',
    description: 'It\'s over nine hundred and ninety-nine!',
    emoji: 'chart_with_upwards_trend'
  },
  youDisgustMe: {
    name: 'You disgust me',
    description: 'Like, wow.',
    emoji: 'nauseated_face'
  },
  bookWorm: {
    name: 'Take a peek at the lore',
    description: 'It\'s gotta be worth your time somehow.',
    emoji: 'books'
  },
  
  theOtherSide: {
    name: 'Die and be reborn',
    description: 'You have seen the other side, and do not fear it.',
    emoji: 'white_square'
  }
}
