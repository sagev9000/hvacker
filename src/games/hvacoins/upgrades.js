const { getCPS, setUpgrades } = require('./utils');

const basic = ({ name, type, description, count, cost, extraCondition = () => true, effect = cps => cps * 2 }) => ({
  name,
  type,
  description,
  condition: (user, squadGrades) => user.items[type] >= count && extraCondition(user, squadGrades),
  cost,
  effect
})

const evil = ({ name, type, description, cost }) => basic({
  name,
  type,
  description,
  count: 40,
  cost,
  extraCondition: (user, squadGrades) => squadGrades?.includes('discardHumanMorals'),
})

const heavenly = ({ name, type, description, cost, multiplier = 2 }) => ({
  name,
  type,
  description,
  condition: (user, squadGrades) => user.items[type] >= 60 && squadGrades?.includes('redemption'),
  cost,
  effect: cps => cps * multiplier
})

const disabled = () => false

const baby = ({ name, type, description, cost }) => basic({
  name,
  type,
  description,
  count: 70,
  cost,
  extraCondition: disabled
})

const geometry = ({ name, type, description, cost }) => basic({
  name,
  type,
  description,
  count: 100,
  cost,
  extraCondition: disabled
})

const universitality = ({ name, type, description, cost }) => basic({
  name,
  type,
  description,
  count: 100,
  cost,
  extraCondition: disabled
})

module.exports = {
  doubleClick: basic({
    name: 'Double-Click',
    type: 'mouse',
    description: 'Doubles the power of mice',
    count: 1,
    cost: 1_000
  }),
  stinkierCheese: basic({
    name: 'Stinkier Cheese',
    type: 'mouse',
    description: 'Mice are doubly motivated to hunt down HVAC Coins',
    count: 10,
    cost: 21_000
  }),
  biggerTeeth: basic({
    name: 'Bigger Teeth',
    type: 'mouse',
    description: 'Mice can intimidate twice as much HVAC out of their victims.',
    count: 25,
    cost: 50_000
  }),
  rats: evil({
    name: 'Rats',
    type: 'mouse',
    description: 'Consume the rotten remains of your foes',
    cost: 150_000,
  }),
  hoodedMice: heavenly({
    name: 'Hooded Mice',
    type: 'mouse',
    description: 'These monks have nearly reached enlightenment. 10x Mouse CPS.',
    cost: 1_000_000,
    multiplier: 10,
  }),
  babyMouse: baby({
    name: 'Baby Mouse',
    type: 'mouse',
    description: 'Squeak!',
    cost: 6_000_000,
  }),

  fasterComputers: basic({
    name: 'Faster Computers',
    type: 'accountant',
    description: 'Accountants can ~steal~ optimize twice as much HVAC!',
    count: 1,
    cost: 11_000,
  }),
  lackOfMorality: basic({
    name: 'Lack of Morality',
    type: 'accountant',
    description: 'Accountants are taking a hint from nearby CEOs.',
    count: 10,
    cost: 200_000,
  }),
  widerBrains: basic({
    name: 'Wider Brains',
    type: 'accountant',
    description: 'For accountant do double of thinking.',
    count: 25,
    cost: 550_000,
  }),
  vastLayoffs: evil({
    name: 'Vast Layoffs',
    type: 'accountant',
    description: 'The weak are not part of our future.',
    cost: 2_450_000,
  }),
  charityFund: heavenly({
    name: 'Charity Fund',
    type: 'accountant',
    description: 'THIS one is more than just a tax break. 9x Accountant CPS.',
    cost: 16_333_333,
    multiplier: 9,
  }),
  mathBaby: baby({
    name: 'Math Baby',
    type: 'accountant',
    description: '2 + 2 = WAAH!',
    cost: 99_999_999,
  }),

  biggerBlowhole: basic({
    name: 'Bigger Blowhole',
    type: 'whale',
    description: 'With all that extra air, whales have double power!',
    count: 1,
    cost: 120_000
  }),
  sassyWhales: basic({
    name: 'Sassy Whales',
    type: 'whale',
    description: 'These are the kind of whales that know how to get twice as much done',
    count: 10,
    cost: 3_000_000
  }),
  thinnerWater: basic({
    name: 'Thinner Water',
    type: 'whale',
    description: 'Whales can move twice as quickly through this physics-defying liquid',
    count: 25,
    cost: 6_000_000
  }),
  blightWhales: evil({
    name: 'Blight Whales',
    type: 'whale',
    description: `Infectious with evil, they swim the ocean spreading their spores.`,
    cost: 24_000_000
  }),
  whaleChoir: heavenly({
    name: 'Whale Choir',
    type: 'whale',
    description: `Their cleansing songs reverberate through the sea. 8x Whale CPS.`,
    cost: 144_000_000,
    multiplier: 8,
  }),
  smolWhales: baby({
    name: 'Smol Whales',
    type: 'whale',
    description: ``,
    cost: 8_400_000_000
  }),

  greasyTracks: basic({
    name: 'Greasy Tracks',
    type: 'train',
    description: 'Lets trains deliver HVAC twice as efficiently',
    count: 1,
    cost: 1_300_000
  }),
  rocketThrusters: basic({
    name: 'Rocket Thrusters',
    type: 'train',
    description: 'That\'ll put some quack on your track',
    count: 10,
    cost: 22_000_000
  }),
  loudConductors: basic({
    name: 'Loud Conductors',
    type: 'train',
    description: 'Conductors can onboard twice as much HVAC',
    count: 25,
    cost: 65_000_000
  }),
  hellTrain: evil({
    name: 'Hell Train',
    type: 'train',
    description: 'Shipping blood needed for the ritual.',
    cost: 370_000_000
  }),
  toyTrain: heavenly({
    name: 'Toy Train',
    type: 'train',
    description: 'Toot toot! 8x Train CPS.',
    multiplier: 8,
    cost: 2_220_000_000
  }),

  gasolineFire: basic({
    name: 'Gasoline Fire',
    type: 'fire',
    description: 'Extremely good for breathing in.',
    count: 1,
    cost: 14_000_000
  }),
  extremelyDryFuel: basic({
    name: 'Extremely Dry Fuel',
    type: 'fire',
    description: 'Hey, psst, hey. Use the ignite command for a secret achievement.',
    count: 10,
    cost: 163_000_000
  }),
  cavemanFire: basic({
    name: 'Caveman Fire',
    type: 'fire',
    description: 'They just don\'t make \'em like they used to.',
    count: 25,
    cost: 700_000_000
  }),
  lava: evil({
    name: 'Lava',
    type: 'fire',
    description: `Hopefully no usurpers have any "accidents".`,
    cost: 4_200_000_000
  }),
  blueFire: heavenly({
    name: 'Blue Fire',
    type: 'fire',
    description: `You can hear it singing with delight. 7x Fire CPS.`,
    multiplier: 7,
    cost: 25_200_000_000
  }),
  cuteFire: baby({
    name: 'Cute Fire',
    type: 'fire',
    description: `I just met my perfect match...`,
    cost: 150_000_000_000
  }),

  spoonerang: basic({
    name: 'Spoonerang',
    type: 'boomerang',
    description: 'Scoops up HVAC mid-flight',
    count: 1,
    cost: 200_000_000
  }),
  boomerAng: basic({
    name: 'Boomer-ang',
    type: 'boomerang',
    description: 'It\'s... old.',
    count: 10,
    cost: 1_200_000_000
  }),
  doubleRang: basic({
    name: 'Double-rang',
    type: 'boomerang',
    description: 'You throw one, but somehow catch two',
    count: 25,
    cost: 10_000_000_000
  }),
  loyalRang: evil({
    name: 'Loyal-rang',
    type: 'boomerang',
    description: `Frequently reports back to your throne on the state of your empire.`,
    cost: 60_000_000_000
  }),
  youRang: heavenly({
    name: 'You-rang',
    type: 'boomerang',
    description: 'Your arms and legs recede into your body. You bend at the middle. You fly. And for a moment, you are free._\n_7x Boomerang CPS.',
    multiplier: 7,
    cost: 360_000_000_000
  }),

  lunarPower: basic({
    name: 'Lunar Power',
    type: 'moon',
    description: 'Out with the sol, in with the lun!',
    count: 1,
    cost: 3_300_000_000
  }),
  womanOnTheMoon: basic({
    name: 'Woman on the Moon',
    type: 'moon',
    description: 'There\'s no reason for it not to be a woman!',
    count: 10,
    cost: 39_700_000_000
  }),
  doubleCraters: basic({
    name: 'Double-Craters',
    type: 'moon',
    description: 'Making every side look like the dark side.',
    count: 25,
    cost: 165_000_000_000
  }),
  tidalUpheaval: evil({
    name: 'Tidal Upheaval',
    type: 'moon',
    description: `The hell with the ocean. That's valuable_ *the abstract concept of more power* _we're losing.`,
    cost: 865_000_000_000
  }),
  newMoon: heavenly({
    name: 'New Moon',
    type: 'moon',
    description: `Build a second moon to provide space for affordable housing. 6x Moon CPS.`,
    multiplier: 6,
    cost: 5_190_000_000_000
  }),

  glassButterfly: basic({
    name: 'Glass Butterfly',
    type: 'butterfly',
    description: 'Not your grandma\'s universe manipulation.',
    count: 1,
    cost: 51_000_000_000
  }),
  monarchMigration: basic({
    name: 'Monarch Migration',
    type: 'butterfly',
    description: 'This upgrade brought to you by milkweed.',
    count: 10,
    cost: 870_000_000_000
  }),
  quadWing: basic({
    name: 'Quad-Wing',
    type: 'butterfly',
    description: 'Sounds a lot like a trillion bees buzzing inside your head.',
    count: 25,
    cost: 2_550_000_000_000
  }),
  venomousMoths: evil({
    name: 'Venomous Moths',
    type: 'butterfly',
    description: 'Specifically manufactured for their horrifying brain-melt toxins.',
    cost: 12_550_000_000_000
  }),
  quietingNectar: heavenly({
    name: 'Quieting Nectar',
    type: 'butterfly',
    description: 'Calming and extra sweet. Soothes even human ails. 6x Butterfly CPS.',
    multiplier: 6,
    cost: 75_300_000_000_000
  }),

  silverMirror: basic({
    name: 'Silver Mirror',
    type: 'mirror',
    description: 'Excellent for stabbing vampires.',
    count: 1,
    cost: 750_000_000_000
  }),
  pocketMirror: basic({
    name: 'Pocket Mirror',
    type: 'mirror',
    description: 'Take your self-reflection on the go!',
    count: 10,
    cost: 18_000_000_000_000
  }),
  window: basic({
    name: 'Window',
    type: 'mirror',
    description: 'Only through looking around you can you acquire the self reflection necessary to control the thermostat.',
    count: 25,
    cost: 37_500_000_000_000
  }),
  crackedMirror: evil({
    name: 'Cracked Mirror',
    type: 'mirror',
    description: `YOU SMILE. DO NOT FEAR, THIS IS THE FACE OF A FRIEND.`,
    cost: 222_000_000_000_000
  }),
  funHouseMirror: heavenly({
    name: 'Fun-House Mirror',
    type: 'mirror',
    description: `yoU LOok so siLLY IN thesE THINgs. 5X mIRror CpS.`,
    multiplier: 5,
    cost: 1_330_000_000_000_000
  }),

  fzero: basic({
    name: 'F-Zero',
    type: 'quade',
    description: 'Brings out his competitive spirit.',
    count: 1,
    cost: 10_000_000_000_000
  }),
  triHumpCamel: basic({
    name: 'Trimedary Camel',
    type: 'quade',
    description: 'YEE HAW :trimedary_camel:',
    count: 10,
    cost: 200_000_000_000_000
  }),
  adam: basic({
    name: 'Adam',
    type: 'quade',
    description: 'He could probably reach the thermostat if he wanted.',
    count: 25,
    cost: 500_000_000_000_000
  }),
  thatsNotQuade: evil({
    name: `That's not Quade...`,
    type: 'quade',
    description: `The skinless face lacks even a moustache. Nevertheless, it pledges its allegiance.`,
    cost: 3_000_000_000_000_000
  }),
  hannahMontanaLinux: heavenly({
    name: 'Hannah Montana Linux',
    type: 'quade',
    description: `The patrician's choice. 4x Quade CPS.`,
    multiplier: 4,
    cost: 18_000_000_000_000_000
  }),

  latestNode: basic({
    name: 'Latest Node',
    type: 'hvacker',
    description: 'The old one has terrible ergonomics, tsk tsk.',
    count: 1,
    cost: 140_000_000_000_000
  }),
  nativeFunctions: basic({
    name: 'Native Functions',
    type: 'hvacker',
    description: 'Sometimes javascript just isn\'t fast enough.',
    count: 10,
    cost: 3_300_000_000_000_000
  }),
  gitCommits: basic({
    name: 'Git Commits',
    type: 'hvacker',
    description: 'The heads of multiple people in a company are better than, for example, merely one head.',
    count: 25,
    cost: 7_000_000_000_000_000
  }),
  undefinedBehavior: evil({
    name: 'Undefined Behavior',
    type: 'hvacker',
    description: `skREEEFDS☐☐☐☐☐it's☐jwtoo☐laate☐☐☐☐☐`,
    cost: 42_000_000_000_000_000
  }),
  mutualUnderstanding: heavenly({
    name: 'Mutual Understanding',
    type: 'hvacker',
    description: `lol fat chance, dummy. Points for trying, though. 3x Hvacker CPS`,
    multiplier: 3,
    cost: 250_000_000_000_000_000
  }),

  coffee: basic({
    name: 'Coffee',
    type: 'creator',
    description: `Didn't you know? It makes you smarter. No consequencAAAAAA`,
    count: 1,
    cost: 1_960_000_000_000_000
  }),
  bribery: basic({
    name: 'Bribery',
    type: 'creator',
    description: `How much could he be making that a couple bucks won't get me more HVAC?`,
    count: 10,
    cost: 32_300_000_000_000_000
  }),
  vim: basic({
    name: 'Vim',
    type: 'creator',
    description: `*teleports behind you*`,
    count: 25,
    cost: 100_000_000_000_000_000
  }),
  regrets: evil({
    name: 'Regrets',
    type: 'creator',
    description: `HE HAS NONE. HE LAUGHS.`,
    cost: 600_000_000_000_000_000
  }),
  goVegan: heavenly({
    name: 'Go Vegan',
    type: 'creator',
    description: `Unlock your vegan powers. 3x Creator CPS.`,
    multiplier: 3,
    cost: 3_600_000_000_000_000_000
  }),

  angelInvestors: basic({
    name: 'Angel Investors',
    type: 'smallBusiness',
    description: 'Not so small NOW are we?',
    count: 1,
    cost: 3_140_000_000_000_000
  }),
  officeManager: basic({
    name: 'Office Manager',
    type: 'smallBusiness',
    description: 'Sate your laborers with snacks.',
    count: 10,
    cost: 80_000_000_000_000_000
  }),
  undyingLoyalty: basic({
    name: 'Undying Loyalty',
    type: 'smallBusiness',
    description: 'Your foolish employees bow to your every whim, regardless of salary.',
    count: 25,
    cost: 138_000_000_000_000_000
  }),
  deathSquad: evil({
    name: 'Death Squad',
    type: 'smallBusiness',
    description: `pwease don't unionize uwu :pleading_face:`,
    cost: 858_000_000_000_000_000
  }),
  coop: heavenly({
    name: 'Co-Op',
    type: 'smallBusiness',
    description: `By the people, for the people. 2x smallBusiness CPS`,
    multiplier: 2,
    cost: 5_140_000_000_000_000_000
  }),

  corporateBuyouts: basic({
    name: 'Corporate Buyouts',
    type: 'bigBusiness',
    description: 'The cornerstone of any family-run business.',
    count: 1,
    cost: 28_140_000_000_000_000
  }),
  politicalSway: basic({
    name: 'Political Sway',
    type: 'bigBusiness',
    description: `What's a bit of lobbying between friends?`,
    count: 10,
    cost: 560_000_000_000_000_000
  }),
  humanDiscontent: basic({
    name: 'Human Discontent',
    type: 'bigBusiness',
    description: 'A sad populace is a spendy populace!',
    count: 25,
    cost: 1_372_000_000_000_000_000
  }),
  weJustKillPeopleNow: evil({
    name: 'We Just Kill People Now',
    type: 'bigBusiness',
    description: 'It is extremely difficult to get more evil than we already were. Nevertheless,',
    cost: 7_072_000_000_000_000_000
  }),
  makePublic: heavenly({
    name: 'Make Public',
    type: 'bigBusiness',
    description: `Downplay immediate profit for more long-term benefits. 2x bigBusiness CPS.`,
    multiplier: 2,
    cost: 42_000_000_000_000_000_000
  }),

  community: basic({
    name: 'Community',
    type: 'government',
    description: `In a sense, this is really all you need.`,
    count: 1,
    cost: 280_140_000_000_000_000
  }),
  theState: basic({
    name: 'The State',
    type: 'government',
    description: 'Congratulations, you have a monopoly on violence.',
    count: 10,
    cost: 5_060_000_000_000_000_000
  }),
  openBorders: basic({
    name: 'Open Borders',
    type: 'government',
    description: 'Cigars, anyone?',
    count: 25,
    cost: 9_999_999_999_999_999_999
  }),
  capitalism: evil({
    name: 'Capitalism',
    type: 'government',
    description: 'Obviously this is the ideal economy no further questions thank you.',
    cost: 70_072_000_000_000_000_000
  }),
  socialism: heavenly({
    name: 'Socialism',
    type: 'government',
    description: `A dictatorship of the proletariat. And thank god.`,
    multiplier: 2,
    cost: 690_000_000_000_000_000_000
  }),

  homage: {
    name: 'Homage',
    type: 'general',
    description: 'The power of original ideas increases your overall CPS by 10%',
    condition: user => Object.entries(user.items).reduce((total, [, countOwned]) => countOwned + total, 0) >= 200,
    emoji: 'cookie',
    cost: 10_000_000_000,
    effect: (itemCps, user) => itemCps * 1.1
  },
  iLoveHvac: {
    name: 'iLoveHvac',
    type: 'general',
    description: 'The power of love increases your overall CPS by 10%',
    condition: user => Object.entries(user.items).reduce((total, [, countOwned]) => countOwned + total, 0) >= 400,
    emoji: 'heart',
    cost: 100_000_000_000_000,
    effect: (itemCps, user) => itemCps * 1.1
  },

  digitalPickaxe: {
    name: 'Digital Pickaxe',
    type: 'mining',
    description: 'Break coinful digirocks into bits, increasing the power of !mine',
    condition: user => user.interactions > 100,
    emoji: 'pick',
    cost: 100_000,
    effect: (mineTotal, user) => mineTotal + (getCPS(user) * 0.1)
  },
  vacuum: {
    name: 'Digital Vacuum',
    type: 'mining',
    description: 'Suck up leftover HVAC dust, greatly increasing the power of !mine',
    condition: user => user.interactions > 500,
    emoji: 'vacuum',
    cost: 10_000_000,
    effect: (mineTotal, user) => mineTotal + (getCPS(user) * 0.1)
  },
  mineCart: {
    name: 'HVAC Mine Cart',
    type: 'mining',
    description: 'You\'d shine like a diamond, down in the !mine',
    condition: user => user.interactions > 1500,
    emoji: 'shopping_trolley',
    cost: 100_000_000_000,
    effect: (mineTotal, user) => mineTotal + (getCPS(user) * 0.1)
  },
  fpga: {
    name: 'FPGA Miner',
    type: 'mining',
    description: 'Wait, what kind of mining is this again?',
    condition: user => user.interactions > 5000,
    emoji: 'floppy_disk',
    cost: 1_000_000_000_000_000,
    effect: (mineTotal, user) => mineTotal + (getCPS(user) * 0.1)
  }
}

setUpgrades(module.exports)
