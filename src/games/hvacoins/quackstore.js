const getRandomFromArray = array => array[Math.floor(Math.random() * array.length)]

const chaosCpsMods = [3, 2, 0.1, 1, 1.5, 1.6, 0, 1.1, 1.1, 1.26]
const chaosAvg = () => chaosCpsMods.reduce((total, next) => total + next, 0) / chaosCpsMods.length
//const getChaos = offset => chaosCpsMods[(Math.floor(new Date().getSeconds() / chaosCpsMods.length) + offset) % chaosCpsMods.length]
const getChaos = offset => chaosCpsMods[(Math.floor(new Date().getSeconds() / chaosCpsMods.length) + offset) % chaosCpsMods.length]

const quackStore = {
  ascent: {
    name: 'Ascent',
    type: 'cps',
    emoji: 'rocket',
    description: 'Welcome to level 2. Boosts all CPS by 20%',
    effect: cps => cps * 1.2,
    cost: 1
  },
  nuclearFuel: {
    name: 'Nuclear Fuel',
    type: 'cps',
    emoji: 'atom_symbol',
    description: 'The future is now, old man. Boosts all CPS by 20%.',
    preReqs: ['ascent'],
    effect: cps => cps * 1.2,
    cost: 5
  },
  chaos: {
    name: 'Chaos',
    type: 'cps',
    emoji: 'eye',
    description: 'Awaken. Gives a random modifier to your CPS every six seconds. May have other consequences...',
      //+ '_\n_Averages a 26% CPS boost.',
    preReqs: ['nuclearFuel'],
    effect: (cps, user) => {
      return cps * getChaos(Math.round(user.interactions / 50))
    },
    cost: 10
  },

  dryerSheet: {
    name: 'Dryer Sheet',
    type: 'lightning',
    emoji: 'rose',
    description: 'Smells nice. Makes lightning twice as likely to strike.',
    effect: lightningOdds => lightningOdds * 2,
    preReqs: ['nuclearFuel'],
    cost: 10
  },

  // Checked Upgrades. Have no effect(), but their existence is referred to elsewhere.
  theGift: {
    name: 'The Gift',
    type: 'checked',
    emoji: 'eye-in-speech-bubble',
    description: 'Become forewarned of certain events...',
    preReqs: ['dryerSheet', 'chaos'],
    cost: 10
  },

  theVoice: {
    name: 'The Voice',
    type: 'checked',
    emoji: 'loud_sound',
    description: 'Unlocks the !speak command',
    preReqs: ['dryerSheet', 'chaos'],
    cost: 50
  },

  cheeseBaby: {
    name: 'cheeseBaby',
    type: 'starter',
    emoji: 'baby_symbol',
    description: 'Start each prestige with 5 mice',
    preReqs: ['ascent'],
    effect: user => {
      user.items.mouse ??= 0
      user.items.mouse += 5
    },
    cost: 4
  },

  silverSpoon: {
    name: 'Silver Spoon',
    type: 'starter',
    emoji: 'spoon',
    description: 'Start each prestige with 5 accountants',
    preReqs: ['cheeseBaby'],
    effect: user => {
      user.items.accountant ??= 0
      user.items.accountant += 5
    },
    cost: 16
  },

  sharkBoy: {
    name: 'Shark Boy',
    type: 'starter',
    emoji: 'ocean',
    description: 'Start each prestige with 5 whales',
    preReqs: ['silverSpoon'],
    effect: user => {
      user.items.whale ??= 0
      user.items.whale += 5
    },
    cost: 64
  },

  superClumpingLitter: {
    name: 'Super-Clumping Cat Litter',
    type: 'pet',
    emoji: 'smirk_cat',
    description: 'Extra-strength pet effects',
    preReqs: ['sharkBoy'],
    effect: petMultiplier => {
      petMultiplier = Math.max(petMultiplier, 1)
      return petMultiplier * petMultiplier
    },
    cost: 128
  },

  magnetMan: {
    name: 'Magnet Man',
    type: 'starter',
    emoji: 'magnet',
    description: 'Start each prestige with 5 Trains',
    preReqs: ['sharkBoy'],
    effect: user => {
      user.items.train ??= 0
      user.items.train += 5
    },
    cost: 256
  },

  catFan: {
    name: 'Cat Fan',
    type: 'pet',
    emoji: 'cat',
    description: 'Super extra-strength pet effects',
    preReqs: ['magnetMan', 'superClumpingLitter'],
    effect: petMultiplier => {
      petMultiplier = Math.max(petMultiplier, 1)
      return petMultiplier * petMultiplier
    },
    cost: 512
  },

  lavaGirl: {
    name: 'Lava Girl',
    type: 'starter',
    emoji: 'volcano',
    description: 'Start each prestige with 5 Fire',
    preReqs: ['magnetMan'],
    effect: user => {
      user.items.fire ??= 0
      user.items.fire += 5
    },
    cost: 1024
  },

  aussie: {
    name: 'Aussie',
    type: 'starter',
    emoji: 'flag-au',
    description: 'Start each prestige with 5 Boomerangs',
    preReqs: ['lavaGirl'],
    effect: user => {
      user.items.boomerang ??= 0
      user.items.boomerang += 5
    },
    cost: 4096
  },
}

module.exports = {
  quackStore,
  getChaos: user => getChaos(user.interactions || 0)
}
