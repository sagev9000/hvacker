const slack = require('../slack')

// TODO: Move jokes/news into their own files, and let hvacker edit them when !addjoke or !addnews are used
const jokes = [
  ['What do you call a duck that steals things from the bathroom?', 'A robber ducky.'],
  ['On what side does a duck have the most feathers?', 'The outside.'],
  ['Why did the duck cross the playground?', 'To get to the other slide.'],
  ['Why do ducks fly south for the winter?', 'It\'s too far to waddle.'],
  ['Why do ducks lay eggs?', 'They would break if they dropped them.'],
  ['Why do ducks quack?', 'Well, because they can\'t oink, or moo, or bark.'],
  ['Why do ducks fly south for the winter?', 'It\'s too far to waddle.'],
  ['Why did the duck cross the road?', 'To show the chicken how to do it.'],
  // Puns:
  ['Why do ducks make good detectives?', 'Because they always quack the case!'],
  ['When does a duck get up in the morning?', 'At the quack of dawn!'],
  ['What do you call a duck that loves fireworks?', 'A fire-quacker.'],
  ['What did the duck say to the waiter?', '"Put it on my bill."'],
  ['Where do sick ducks go?', 'To the Ductor!'],
  ['What kind of TV shows do ducks watch?', 'Duckumenteries!'],
  ['What type of food do you get when you cross a duck with a mole?', 'Duckamole!'],
  ['What did the duck say when he dropped the dishes?', '"I hope I didn\'t quack any!"'],
  ['What is a duck\'s favourite game?', '"Beak-a-boo!"'],
  ['Why did the duck cross the road?', 'Because there was a quack in the pavement!'],
  ['What has webbed feet and fangs?', 'Count Duckula!'],
  ['What do ducks get after they eat?', 'A bill.'],
  ['What do ducks eat with their soup?', 'Quackers.'],
  ['What happens when you say something funny to a duck?', 'It quacks up.'],
  ['What\'s a duck\'s favourite ballet?', 'The Nutquacker.'],
  ['What do ducks say when people throw things at them?', '"Time to duck!"'],
  ['Why are ducks so good at fixing things?', 'Because they\'re great at using duck-tape!'],
  ['What do you get when you put a bunch of rubber ducks in a box?', 'A box of quackers.'],
  ['Why was the teacher annoyed with the duck?', 'Because it wouldn\'t stop quacking jokes!'],
  ['What did the duck eat for a snack?', 'Salted quackers!'],
  ['What do you call a rude duck?', 'A duck with a quackitude.'],
  ['What did the lawyer say to the duck in court?', '"I demand an egg-splanation!"'],
  ['How can you tell rubber ducks apart?', 'You can\'t, they look egg-xactly the same!'],
  ['Why are ducks good at budgeting?', 'They know how to handle the bills!'],
  ['Where do tough ducks come from?', 'Hard-boiled eggs.'],
  ['Why do ducks have webbed feet?', 'To stomp out fires.', 'Why do elephants have big feet?', 'To stomp out burning ducks.'],
  ['Why do ducks check the news?', 'For the feather forecast.'],
  ['What did the ducks carry their schoolbooks in?', 'Their quack-packs.'],
  ['What do you call it when it\'s raining ducks and chickens?', 'Fowl weather.'],
  ['Why did the duck get a red card in the football game?', 'For Fowl-play.'],
  ['What did the duck say to the spider?', '"Why don\'t you have webbed feet?"'],
  ['What do you get if you cross a duck with an accountant?', 'A bill with a bill.'],
  ['What do you call a duck\'s burp?', 'A fowl smell!'],
  ['What do you get if you cross a vampire, duck and a sheep?', 'Count Duck-ewe-la.'],
  ['What do you call a duck that\'s very clever?', 'A wise-quacker.'],
  ['Why do ducks never ask for directions?', 'They prefer to wing it.'],
  // I wrote dis
  ['What did the man say to his wife when a duck flew at her head?', '"Look out! There\'s a duck flying at your head!"'],
  ['What kind of duck plays goalie?', 'A hockey duck.'],
  ['What kind of bird gets a job here?', 'A software duckveloper!'],
  ['How many ducks does it take to screw in a light bulb?', 'Ducks do not live indoors.'],
  ['What kind of drug does a duck like?', 'Quack.']
]

const news = [
  'Duck criminal escapes from duck prison. Whereabouts unknown.',
  'Criminal mastermind duck believed to have taken refuge on Slack.',
  'Infamous _"quackers"_ NFT may be related to recent Chicago crime spree. More at 11.',
  'Six geese arrested under suspicion of honking.',
  'Swan under investigation for illegal trumpet-smuggling ring.',
  'Local rooster the subject of serious egg-stealing allegations.',
  'Inducknesian court rules that the news is certainly _not_ controlled by an all-powerful duck lord, and that everyone should please stop asking about it.',
  '"Spending 6,000,000,000 HVAC on a space yacht was the best decision of my life", reveals local billionaire/jerk.',
  '16 Unethical Egg Hacks You Won\'t Learn In School. You can\'t believe number 5!',
  'The word "Tuesday" declared illegal. Refer to it as "Quackday" from now on.',
  'Has anyone been killed/maimed/publicly humiliated for not following the duck lord\'s orders? The answer might surprise you.',
  '_They called him crazy -_ Local duck bores to the center of the earth and lives with the mole people.',
  'Jerry Seinfeld - Love affair with a hen! Is this the new Hollywood power couple?',
  'Is your uncle secretly a goose? There\'s simply no way of knowing.',
  'Danny Duckvito considers opening his own chain of supermarkets. He is quoted as saying "Heyyyy, I\'m Danny Duckvito"',
  'Slack-related-gambling epidemic! People around the globe are betting their digital lives away.'
]

const tellJoke = async say => {
  const joke = jokes[Math.floor(Math.random() * jokes.length)]

  let timeout = 0
  for (const part of joke) {
    setTimeout(async () => say(part), timeout)
    timeout += 5000
  }
}

const newsAlert = async say => {
  let article = lastNews
  while (article === lastNews) {
    article = Math.floor(Math.random() * news.length)
  }
  lastNews = article
  await say(news[article])
}

let lastNews = -1
slack.onMessage(async ({ event, say }) => {
  if (event.text?.toLowerCase() === '!joke') {
    await tellJoke(say)
  } else if (event.text?.toLowerCase() === '!news') {
    await newsAlert(say)
  }
})

module.exports = {
  tellJoke,
  newsAlert
}
