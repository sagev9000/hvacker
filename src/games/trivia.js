const axios = require('axios')

const getTrivia = async () => axios.get('https://opentdb.com/api.php?amount=10&category=9&difficulty=medium&type=multiple', {
  headers: {
    Accept: 'application/json, text/plain, */*'
  }
})
  .then(res => res.data.results)
  .catch(e => console.error('trivia error', e))

module.exports = {
  getTrivia
}
