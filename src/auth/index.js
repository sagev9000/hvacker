const base64 = require('base-64')
const config = require('../config')

const url = ''

const headers = new Headers()
headers.append('Authorization', 'Basic ' + base64.encode(config.honeywellKey + ':' + config.honeywellSecret))

fetch(url, {
  method: 'GET',
  headers: headers
  // credentials: 'user:passwd'
}).then(response => response.json())
  .then(json => console.log('json', json))
