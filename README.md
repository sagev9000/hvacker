# Hvacker

A Slack-based idle game inspired by (and/or ripping off) Cookie Clicker.

Endlessly self-referential and full of outdated or otherwise 100% unfunny in-jokes, the game has
actually ended up pretty playable. Progression is classic diminishing-returns idler, and a few
social elements keep things from getting stale too quickly.

It also features (among other things) a simple poll system for voting on thermostat controls (hence
the name, HVACker) and was originally intended to control a thermostat directly, but the system on
hand ended up lacking the appropriate API support.

## Design Philosophy

Unlike most game development frameworks, Slack has significant rate-limiting depending on the
actions you want to take. Thus, it was decided that as few elements as possible should actually
update in real time. Instead of processing game ticks as frequently as possible, they are handled
only at request time. Essentially, using a very large delta value between ticks.

This also leaves the Hvacker server relatively lightweight, in an idle state at nearly all times.